package ru.vorax.properties;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component
public class ChatNodeProperties {

    @Value("${shardId}")
    private String shardId;

    @Value("${shardHost}")
    private String shardHost;

    @Value("${shardPort}")
    private Integer shardPort;
}
