package ru.vorax.properties;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component
public class ChatEventProperties {

    @Value("${control-node-url}")
    private String controlNodeUrl;

}
