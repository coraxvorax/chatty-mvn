package ru.vorax.chat;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;
import ru.vorax.chat.connector.ChatConnector;
import ru.vorax.chat.message.ChatMessageParser;
import ru.vorax.chat.message.MessagePusher;
import ru.vorax.properties.ChatNodeProperties;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.concurrent.Executors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ChatNode implements InitializingBean {

    private Selector selector;
    private InetSocketAddress address;

    private final ChatNodeProperties chatNodeProperties;
    private final ChatConnector chatConnector;
    private final MessagePusher messagePusher;
    private final ChatMessageParser chatMessageParser;

    @Override
    public void afterPropertiesSet() {
        address = new InetSocketAddress(chatNodeProperties.getShardHost(), chatNodeProperties.getShardPort());
        Executors.newSingleThreadExecutor()
                .submit(() -> {
                    this.selector = Selector.open();
                    try (ServerSocketChannel serverSocketChannel = ServerSocketChannel.open()) {
                        serverSocketChannel.bind(address);
                        serverSocketChannel.configureBlocking(false);
                        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
                        LOG.info("Server started...");
                        while (true) {
                            selector.select();
                            Iterator<SelectionKey> keys = selector.selectedKeys().iterator();
                            while (keys.hasNext()) {
                                SelectionKey key = keys.next();
                                keys.remove();
                                if (!key.isValid()) {
                                    continue;
                                }
                                if (key.isAcceptable()) {
                                    acceptOf(key);
                                }
                                if (key.isReadable()) {
                                    readFrom(key);
                                }
                            }
                        }
                    }
                });
    }

    private void acceptOf(SelectionKey key) throws IOException {
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
        SocketChannel channel = serverSocketChannel.accept();
        channel.configureBlocking(false);
        channel.register(selector, SelectionKey.OP_READ);
    }

    private void readFrom(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
        int numRead;
        try {
            numRead = channel.read(byteBuffer);
            if (numRead == -1) {
                chatConnector.removeFromChat(key, channel);
                return;
            }
        } catch (Exception ex) {
            chatConnector.removeFromChat(key, channel);
            return;
        }

        byte[] data = new byte[numRead];
        System.arraycopy(byteBuffer.array(), 0, data, 0, numRead);
        chatMessageParser.getChatMessage(data)
                .ifPresent(chatMessage -> {
                    switch (chatMessage.getType()) {
                        case CONNECT:
                            LOG.info("Registration: {}", chatMessage);
                            chatConnector.addToChat(channel, chatMessage.getUser());
                            break;
                        case RAW_MESSAGE:
                            LOG.info("Send message from: {}", channel.socket().getRemoteSocketAddress());
                            messagePusher.pushMessage(chatMessage);
                            break;
                        case BROADCAST_MESSAGE:
                            LOG.info("Broadcast from: {}", channel.socket().getRemoteSocketAddress());
                            messagePusher.pushBroadcastMessage(chatMessage);
                            break;
                        default:
                            LOG.info("Unknown type from: {}", channel.socket().getRemoteSocketAddress());
                    }
                });
    }
}