package ru.vorax.chat.message;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;
import ru.vorax.cache.ActiveUser;
import ru.vorax.cache.ActiveUsersCache;

@Slf4j
@Component
@RequiredArgsConstructor
public class DefaultMessagePusher implements MessagePusher {

    private final RedissonClient redissonClient;
    private final ActiveUsersCache activeUsersCache;

    @Override
    public void pushMessage(ChatMessage chatMessage) {
        try {
            String recipientShardId = resolveRecipientShard(chatMessage.getRecipient());
            String shardTopicName = createShardTopicName(recipientShardId, "pushMessages");
            RTopic<TopicMessage> subscribeTopic = redissonClient.getTopic(shardTopicName);
            subscribeTopic.publish(new TopicMessage(chatMessage.getUser(), chatMessage.getRecipient(),
                    chatMessage.getUser() + ": " + chatMessage.getBody() + "\n"));
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    private String resolveRecipientShard(String recipient) {
        return activeUsersCache.getActiveUser(recipient)
                .map(ActiveUser::getShardId)
                .orElseThrow(() -> new IllegalArgumentException("Unknown recipient:" + recipient));
    }

    private static String createShardTopicName(String recipientShardId, String topicType) {
        return topicType + ".shard" + recipientShardId;
    }

    @Override
    public void pushBroadcastMessage(ChatMessage chatMessage) {
        //TODO: resolve shards of active users
        //TODO: create name of queue
        //TODO: push message to queue
        RTopic<BroadcastTopicMessage> broadcastTopic = redissonClient.getTopic("broadcastMessages");
        broadcastTopic.publish(new BroadcastTopicMessage(
                chatMessage.getUser(), chatMessage.getUser() + ": " + chatMessage.getBody() + "\n"));
    }

    @Override
    public void pushBroadcastMessage(String sender, String message) {
        //TODO: resolve shards of active users
        //TODO: create name of queues
        //TODO: push message to queue
        RTopic<BroadcastTopicMessage> broadcastTopic = redissonClient.getTopic("broadcastMessages");
        broadcastTopic.publish(new BroadcastTopicMessage(sender, message));
    }
}
