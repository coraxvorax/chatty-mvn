package ru.vorax.chat.message;

public interface MessagePusher {
    void pushMessage(ChatMessage chatMessage);

    void pushBroadcastMessage(ChatMessage chatMessage);

    void pushBroadcastMessage(String sender, String message);
}
