package ru.vorax.chat.message;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class ChatMessageParser {

    private final ObjectMapper objectMapper;

    public Optional<ChatMessage> getChatMessage(byte[] data) {
        try {
            return Optional.of(objectMapper.readValue(data, ChatMessage.class));
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            return Optional.empty();
        }
    }
}
