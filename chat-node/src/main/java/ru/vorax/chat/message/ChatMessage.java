package ru.vorax.chat.message;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class ChatMessage {
    private Type type;
    private String user;
    private String recipient;
    private String body;

}
