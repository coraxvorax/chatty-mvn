package ru.vorax.chat.message;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;
import ru.vorax.cache.ActiveUsersCache;
import ru.vorax.chat.connector.ChatConnections;
import ru.vorax.event.ChatEventNotifier;
import ru.vorax.event.EventType;
import ru.vorax.properties.ChatNodeProperties;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.ByteBuffer;

@Slf4j
@Component
@RequiredArgsConstructor
public class MessageSender {

    private final ChatNodeProperties chatNodeProperties;
    private final ChatConnections chatConnections;
    private final RedissonClient redissonClient;

    private final ActiveUsersCache activeUsersCache;
    private final ChatEventNotifier chatEventNotifier;

    public void sendMessage(String sender, String recipient, String data) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
        byteBuffer.put(data.getBytes());
        byteBuffer.flip();
        this.chatConnections.getSessions().entrySet().stream()
                .filter(entry -> entry.getValue().equals(recipient))
                .forEach(userChannel -> {
                    try {
                        userChannel.getKey().write(byteBuffer);
                        byteBuffer.flip();
                    } catch (IOException e) {
                        LOG.error(e.getMessage(), e);
                    }
                });
        chatEventNotifier.pushEvent(sender, EventType.SEND_MESSAGE);
        activeUsersCache.updateActiveUser(sender, chatNodeProperties.getShardId());
    }

    public void broadcast(String sender, String data) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
        byteBuffer.put(data.getBytes());
        byteBuffer.flip();
        this.chatConnections.getSessions().forEach((key, value) -> {
            try {
                key.write(byteBuffer);
                byteBuffer.flip();
            } catch (IOException e) {
                LOG.error(e.getMessage(), e);
            }
        });
        chatEventNotifier.pushEvent(sender, EventType.BROADCAST_MESSAGE);
        activeUsersCache.updateActiveUser(sender, chatNodeProperties.getShardId());
    }

    @PostConstruct
    public void init() {
        RTopic<TopicMessage> subscribeTopic =
                redissonClient.getTopic(createShardTopicName("pushMessages", chatNodeProperties.getShardId()));
        subscribeTopic.addListener((channel, topicMessage) ->
                sendMessage(topicMessage.getSender(), topicMessage.getRecipient(), topicMessage.getMessage()));

        RTopic<BroadcastTopicMessage> broadcastTopic = redissonClient.getTopic("broadcastMessages");
        broadcastTopic.addListener((channel, broadcastTopicMessage) ->
                broadcast(broadcastTopicMessage.getSender(), broadcastTopicMessage.getMessage()));
    }

    private static String createShardTopicName(String topicType, String shardId) {
        return topicType + ".shard" + shardId;
    }
}
