package ru.vorax.chat.message;

public enum Type {
    CONNECT, RAW_MESSAGE, BROADCAST_MESSAGE
}
