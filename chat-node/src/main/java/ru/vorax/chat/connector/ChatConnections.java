package ru.vorax.chat.connector;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.nio.channels.SocketChannel;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Getter
@Component
@RequiredArgsConstructor
public class ChatConnections {

    private final Map<SocketChannel, String> sessions = new ConcurrentHashMap<>();

}
