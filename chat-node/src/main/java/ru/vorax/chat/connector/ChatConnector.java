package ru.vorax.chat.connector;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.vorax.cache.ActiveUsersCache;
import ru.vorax.chat.message.MessagePusher;
import ru.vorax.properties.ChatNodeProperties;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

@Slf4j
@Component
@RequiredArgsConstructor
public class ChatConnector {

    private final ChatNodeProperties chatNodeProperties;
    private final ChatConnections chatConnections;
    private final MessagePusher messagePusher;
    private final ActiveUsersCache activeUsersCache;

    public void addToChat(SocketChannel channel, String user) {
        this.chatConnections.getSessions().put(channel, user);
        activeUsersCache.updateActiveUser(user, chatNodeProperties.getShardId());
        messagePusher.pushBroadcastMessage(user, "User connected: " + user + "\n");
    }

    public void removeFromChat(SelectionKey key, SocketChannel channel) throws IOException {
        String username = this.chatConnections.getSessions().get(channel);
        this.chatConnections.getSessions().remove(channel);
        LOG.info("Remove from chat: " + channel.socket().getRemoteSocketAddress());
        messagePusher.pushBroadcastMessage(username, "User left: " + username + "\n");
        channel.close();
        key.cancel();
    }
}
