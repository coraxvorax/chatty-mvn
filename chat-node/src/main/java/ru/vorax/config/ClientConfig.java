package ru.vorax.config;

import com.ning.http.client.AsyncHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClientConfig {

    @Bean
    public AsyncHttpClient asyncHttpClient() {
        return new AsyncHttpClient();
    }
}
