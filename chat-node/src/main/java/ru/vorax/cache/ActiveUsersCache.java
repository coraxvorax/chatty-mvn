package ru.vorax.cache;

import java.util.List;
import java.util.Optional;

public interface ActiveUsersCache {

    List<ActiveUser> getActiveUsers();

    Optional<ActiveUser> getActiveUser(String login);

    void updateActiveUser(String login, String shardId);
}
