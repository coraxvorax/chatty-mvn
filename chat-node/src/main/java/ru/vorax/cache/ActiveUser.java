package ru.vorax.cache;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActiveUser implements Serializable {
    private String login;
    private String shardId;
    private Long lastActivityTimestamp;
}
