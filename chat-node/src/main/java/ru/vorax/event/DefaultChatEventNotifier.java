package ru.vorax.event;

import com.ning.http.client.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.vorax.properties.ChatEventProperties;

@Slf4j
@Component
@RequiredArgsConstructor
public class DefaultChatEventNotifier implements ChatEventNotifier {

    private final AsyncHttpClient asyncHttpClient;
    private final ChatEventProperties chatEventProperties;

    @Override
    public void pushEvent(String sender, EventType eventType) {
        LOG.info("Push event:sender={}, eventType={}", sender, eventType);
        asyncHttpClient.executeRequest(createRequest(sender, eventType), createResponseHandler());
    }

    private AsyncCompletionHandler<Response> createResponseHandler() {
        return new AsyncCompletionHandler<Response>() {
            public Response onCompleted(Response response) {
                LOG.info("Successfully send");
                return response;
            }

            public void onThrowable(Throwable t) {
                LOG.error(t.getMessage());
            }
        };
    }

    private Request createRequest(String login, EventType eventType) {
        return new RequestBuilder("POST")
                .setUrl(chatEventProperties.getControlNodeUrl() + "/api/v1/chat/events")
                .addQueryParam("login", login)
                .addQueryParam("type", eventType.name())
                .build();
    }
}
