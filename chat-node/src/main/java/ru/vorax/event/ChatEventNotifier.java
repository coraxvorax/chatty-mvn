package ru.vorax.event;

public interface ChatEventNotifier {
    void pushEvent(String sender, EventType sendMessage);
}
