package ru.vorax.event;

public enum EventType {
    SEND_MESSAGE, BROADCAST_MESSAGE
}
