package ru.vorax.message;

public class ConsoleMessageParser {

    public static ConsoleMessageType resolveType(String message) {
        String[] messageParts = message.split(" ");
        if (messageParts[0].matches("@.*")) {
            return ConsoleMessageType.MESSAGE_TO_RECIPIENT;
        } else if (messageParts[0].matches("\\/.*")) {
            return ConsoleMessageType.COMMAND;
        } else {
            return ConsoleMessageType.BROADCAST;
        }
    }

    public static String extractRecipient(String consoleMessage) {
        return extractHeadOf(consoleMessage);
    }

    public static String extractCommand(String consoleMessage) {
        return extractHeadOf(consoleMessage);
    }

    private static String extractHeadOf(String consoleMessage) {
        String[] messageParts = consoleMessage.split(" ");
        return messageParts[0].substring(1);
    }

    public static String extractMessageToRecipient(String consoleMessage) {
        String[] messageParts = consoleMessage.split(" ");
        StringBuilder message = new StringBuilder();
        for (int i = 1; i < messageParts.length; i++) {
            message.append(messageParts[i]).append(" ");
        }
        return message.toString();
    }
}
