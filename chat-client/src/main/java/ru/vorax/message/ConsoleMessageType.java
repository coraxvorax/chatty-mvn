package ru.vorax.message;

public enum ConsoleMessageType {
    MESSAGE_TO_RECIPIENT, BROADCAST, COMMAND
}
