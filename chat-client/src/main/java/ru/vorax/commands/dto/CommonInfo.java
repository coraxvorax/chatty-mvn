package ru.vorax.commands.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonInfo {
    private String login;
    private String username;
    private String address;
    private Long countOfMessages;
}
