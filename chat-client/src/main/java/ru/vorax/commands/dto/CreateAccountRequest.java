package ru.vorax.commands.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateAccountRequest {
    private String login;
    private String username;
    private String address;
}
