package ru.vorax.commands.client;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component
public class CommandsClientProperties {

    @Value("${control-node-url}")
    private String controlNodeURL;
}
