package ru.vorax.commands.client;

import ru.vorax.commands.dto.AccountsInfoResponse;
import ru.vorax.commands.dto.CreateAccountRequest;

import java.util.Optional;

public interface CommandsClient {

    void createAccount(CreateAccountRequest createAccountRequest);

    AccountsInfoResponse getActiveAccounts();

    Optional<AccountsInfoResponse> getAccount(String login);
}
