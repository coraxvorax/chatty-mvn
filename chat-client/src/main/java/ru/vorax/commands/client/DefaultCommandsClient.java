package ru.vorax.commands.client;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import ru.vorax.commands.dto.AccountsInfoResponse;
import ru.vorax.commands.dto.CreateAccountRequest;

import java.util.Collections;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class DefaultCommandsClient implements CommandsClient {

    private final RestTemplate restTemplate;
    private final CommandsClientProperties commandsClientProperties;

    @Override
    public void createAccount(CreateAccountRequest createAccountRequest) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<CreateAccountRequest> entity = new HttpEntity<>(createAccountRequest, headers);

        //TODO: exception handling
        restTemplate.exchange(commandsClientProperties.getControlNodeURL() + "/api/v1/account",
                HttpMethod.POST, entity, String.class);
    }

    @Override
    public AccountsInfoResponse getActiveAccounts() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<AccountsInfoResponse> response =
                restTemplate.exchange(commandsClientProperties.getControlNodeURL() + "/api/v1/account/active",
                        HttpMethod.GET, entity, AccountsInfoResponse.class);
        //TODO: exception handling
        return response.getBody();
    }

    @Override
    public Optional<AccountsInfoResponse> getAccount(String login) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        try {
            ResponseEntity<AccountsInfoResponse> response =
                    restTemplate.exchange(commandsClientProperties.getControlNodeURL() + "/api/v1/account/" + login,
                            HttpMethod.GET, entity, AccountsInfoResponse.class);
            if (response.getStatusCode().is2xxSuccessful()) {
                return Optional.ofNullable(response.getBody());
            }
            return Optional.empty();
        } catch (HttpClientErrorException ex) {
            if (HttpStatus.valueOf(ex.getRawStatusCode()) == HttpStatus.NOT_FOUND) {
                return Optional.empty();
            }
            throw ex;
        }
    }
}
