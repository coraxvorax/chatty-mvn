package ru.vorax.commands;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.vorax.commands.client.CommandsClient;
import ru.vorax.commands.dto.CommonInfo;
import ru.vorax.commands.dto.CreateAccountRequest;

import java.util.Arrays;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class ChatCommandProcessor {

    private final CommandsClient commandsClient;

    public boolean isUserExist(String login) {
        return commandsClient.getAccount(login).isPresent();
    }

    public void createUser(String login, String userName, String address) {
        commandsClient.createAccount(new CreateAccountRequest(login, userName, address));
    }

    public void sendCommand(String login, String extractedCommand) {
        switch (ChatCommands.commandOf(extractedCommand)) {
            case GET_ACTIVE_USERS:
                String activeUsers = commandsClient.getActiveAccounts().getCommonInfos().stream()
                        .map(CommonInfo::getLogin)
                        .collect(Collectors.joining(", "));
                System.out.println("Active users: " + activeUsers);
                break;
            case GET_DETAILED_ACTIVE_USERS:
                commandsClient.getActiveAccounts().getCommonInfos()
                        .forEach(System.out::println);
                break;
            default:
                LOG.warn("Unknown chat command type");
        }
    }

    @Getter
    @RequiredArgsConstructor
    enum ChatCommands {
        GET_ACTIVE_USERS("active-users"),
        GET_DETAILED_ACTIVE_USERS("active-users-detailed"),
        HELP("help");

        private final String commandText;

        static ChatCommands commandOf(String commandText) {
            return Arrays.stream(ChatCommands.values())
                    .filter(chatCommands -> chatCommands.getCommandText().equals(commandText))
                    .findFirst()
                    .orElse(ChatCommands.HELP);
        }
    }
}
