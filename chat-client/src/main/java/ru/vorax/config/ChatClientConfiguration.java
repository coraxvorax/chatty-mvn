package ru.vorax.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.vorax.chat.client.ChattyClient;

import java.io.IOException;

@Configuration
public class ChatClientConfiguration {

    @Value("${chat-node-host}")
    private String chatNodeHost;

    @Value("${chat-node-port}")
    private Integer chatNodePort;

    @Bean
    public ChattyClient chattyClient() throws IOException {
        return new ChattyClient(chatNodeHost, chatNodePort, System.out::println);
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
