package ru.vorax;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import ru.vorax.chat.ChatSender;
import ru.vorax.chat.ChatUserConnector;
import ru.vorax.commands.ChatCommandProcessor;
import ru.vorax.message.ConsoleMessageType;

import java.util.Scanner;

import static ru.vorax.message.ConsoleMessageParser.*;


@Slf4j
@Component
@RequiredArgsConstructor
@SpringBootApplication
public class ChatConsoleApplication {

    private final ChatUserConnector chatUserConnector;
    private final ChatSender chatSender;
    private final ChatCommandProcessor chatCommandProcessor;

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ChatConsoleApplication.class, args);
        context.getBean(ChatConsoleApplication.class).execute();
    }

    public void execute() {
        System.out.println("Enter login: ");
        try (Scanner scanner = new Scanner(System.in)) {
            String login = scanner.nextLine();
            chatUserConnector.tryToConnect(login, () -> {
                System.out.println("Try to create new user:\nEnter please username:");
                String username = scanner.nextLine();
                System.out.println("Enter please address:");
                String address = scanner.nextLine();
                chatCommandProcessor.createUser(login, username, address);
            });
            while (true) {
                String message = scanner.nextLine();
                ConsoleMessageType consoleMessageType = resolveType(message);
                switch (consoleMessageType) {
                    case MESSAGE_TO_RECIPIENT:
                        chatSender.sendMessage(login, extractRecipient(message), extractMessageToRecipient(message));
                        break;
                    case COMMAND:
                        chatCommandProcessor.sendCommand(login, extractCommand(message));
                        break;
                    case BROADCAST:
                        chatSender.sendBroadcast(login, message);
                        break;
                    default:
                        LOG.warn("Unknown type");
                }
            }
        } catch (JsonProcessingException e) {
            LOG.error(e.getMessage(), e);
        }
    }
}