package ru.vorax.chat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.vorax.chat.client.ChattyClient;
import ru.vorax.chat.dto.ChatMessage;
import ru.vorax.chat.dto.Type;

import static java.lang.String.format;

@Slf4j
@Component
@RequiredArgsConstructor
public class ChatSender {

    private final ObjectMapper objectMapper;
    private final ChattyClient chattyClient;

    public void sendMessage(String sender, String recipient, String message) throws JsonProcessingException {
        LOG.debug(format("Send message:sender=%s, recipient=%s, message=%s", sender, recipient, message));
        chattyClient.sendMessage(objectMapper.writeValueAsString(ChatMessage.builder()
                .type(Type.RAW_MESSAGE)
                .body(message)
                .recipient(recipient)
                .user(sender)
                .build()));
    }

    public void sendBroadcast(String sender, String message) throws JsonProcessingException {
        LOG.debug(format("Broadcast message:sender=%s, message=%s", sender, message));
        chattyClient.sendMessage(objectMapper.writeValueAsString(ChatMessage.builder()
                .type(Type.BROADCAST_MESSAGE)
                .body(message)
                .user(sender)
                .build()));
    }
}
