package ru.vorax.chat.client;

public interface ChatMessageHandler {
    void handle(String message);
}
