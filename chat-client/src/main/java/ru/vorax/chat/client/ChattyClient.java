package ru.vorax.chat.client;

import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.Executors;

import static java.lang.System.arraycopy;
import static java.nio.channels.SelectionKey.*;

@Slf4j
public class ChattyClient {

    private static final String MESSAGE_DELIMITER = "\r\n";

    private final LinkedList<String> messages = new LinkedList<>();
    private final ChatMessageHandler handler;

    private final InetSocketAddress address;
    private final ByteBuffer buffer;
    private SocketChannel channel;
    private Selector selector;

    public ChattyClient(String hostname, int port, ChatMessageHandler handler) throws IOException {
        this.address = new InetSocketAddress(hostname, port);
        this.buffer = ByteBuffer.allocate(8192);
        this.handler = handler;
    }

    @PostConstruct
    public void init() {
        Executors.newSingleThreadExecutor()
                .submit(() -> {
                    //TODO: refactoring. Separate to classes
                    try {
                        channel = SocketChannel.open();
                        channel.configureBlocking(false);
                        channel.connect(address);
                        selector = Selector.open();
                        channel.register(selector, OP_CONNECT);

                        while (true) {
                            selector.select(1000);
                            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                            while (iterator.hasNext()) {
                                SelectionKey key = iterator.next();
                                iterator.remove();
                                if (!key.isValid()) {
                                    continue;
                                }

                                if (key.isConnectable()) {
                                    SocketChannel chan = (SocketChannel) key.channel();
                                    try {
                                        chan.finishConnect();
                                        chan.configureBlocking(false);
                                        chan.register(selector, OP_WRITE);
                                    } catch (IOException e) {
                                        key.channel().close();
                                        key.cancel();
                                    }
                                } else if (key.isAcceptable()) {
                                    ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
                                    SocketChannel socketChannel = serverSocketChannel.accept();
                                    socketChannel.configureBlocking(false);
                                    socketChannel.register(selector, OP_READ);
                                } else if (key.isReadable()) {
                                    SocketChannel chan = (SocketChannel) key.channel();
                                    buffer.clear();
                                    int readCount;
                                    try {
                                        readCount = chan.read(buffer);
                                    } catch (IOException e) {
                                        key.cancel();
                                        chan.close();
                                        return;
                                    }
                                    if (readCount == -1) {
                                        key.channel().close();
                                        key.cancel();
                                        return;
                                    }
                                    byte[] data = new byte[buffer.position()];
                                    arraycopy(buffer.array(), 0, data, 0, buffer.position());
                                    handleIncomingData(key, data);
                                } else if (key.isWritable()) {
                                    SocketChannel chan = (SocketChannel) key.channel();
                                    while (!messages.isEmpty()) {
                                        String message = messages.poll();
                                        chan.write(ByteBuffer.wrap(message.getBytes()));
                                    }
                                    key.interestOps(OP_READ);
                                }
                            }

                        }
                    } catch (Exception e) {
                    } finally {
                        for (SelectionKey key : selector.keys()) {
                            close(key.channel());
                        }
                        close(selector);
                    }
                });
    }

    private static void close(Closeable... closeables) {
        try {
            for (Closeable closeable : closeables)
                closeable.close();
        } catch (IOException e) {
        }
    }

    private void handleIncomingData(SelectionKey sender, byte[] data) {
        for (String message : new String(data).split(MESSAGE_DELIMITER)) {
            handler.handle(message);
        }
    }

    public void sendMessage(String message) {
        messages.add(message + MESSAGE_DELIMITER);
        SelectionKey key = channel.keyFor(selector);
        key.interestOps(OP_WRITE);
    }
}
