package ru.vorax.chat.dto;

public enum Type {
    CONNECT, RAW_MESSAGE, BROADCAST_MESSAGE
}
