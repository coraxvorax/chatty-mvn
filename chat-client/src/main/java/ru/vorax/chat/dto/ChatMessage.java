package ru.vorax.chat.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class ChatMessage {
    private Type type;
    private String user;
    private String recipient;
    private String body;
}
