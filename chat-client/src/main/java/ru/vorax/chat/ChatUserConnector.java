package ru.vorax.chat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.vorax.chat.client.ChattyClient;
import ru.vorax.chat.dto.ChatMessage;
import ru.vorax.chat.dto.Type;
import ru.vorax.commands.ChatCommandProcessor;

@Slf4j
@Component
@RequiredArgsConstructor
public class ChatUserConnector {

    private final ObjectMapper objectMapper;
    private final ChattyClient chattyClient;
    private final ChatCommandProcessor chatCommandProcessor;

    public void tryToConnect(String login, FunctionForRegistration registration) throws JsonProcessingException {
        if (!chatCommandProcessor.isUserExist(login)) {
            LOG.info("Try to create user " + login);
            registration.apply();
        }
        LOG.info("Try to connect: user = " + login);
        chattyClient.sendMessage(objectMapper.writeValueAsString(ChatMessage.builder()
                .type(Type.CONNECT)
                .user(login)
                .build()));
    }

    public interface FunctionForRegistration {
        void apply();
    }
}
