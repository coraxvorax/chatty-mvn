package ru.vorax.storage.model;

public enum ChatEventType {
    SEND_MESSAGE, BROADCAST_MESSAGE
}
