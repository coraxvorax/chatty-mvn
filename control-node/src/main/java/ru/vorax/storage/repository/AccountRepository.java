package ru.vorax.storage.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.vorax.storage.model.Account;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findByLogin(String login);

    @Query(value = "SELECT ac FROM Account ac WHERE ac.login IN :logins")
    List<Account> findByLogins(@Param("logins") Collection<String> logins);
}
