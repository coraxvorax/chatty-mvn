package ru.vorax.storage.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.vorax.storage.model.ChatEvent;

@Repository
public interface ChatEventRepository extends JpaRepository<ChatEvent, Long> {

    Long countByAccountLogin(String accountLogin);
}
