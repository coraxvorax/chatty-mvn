package ru.vorax.cache;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class DefaultActiveUsersCache implements ActiveUsersCache {

    private final RedissonClient redissonClient;

    @Override
    public List<ActiveUser> getActiveUsers() {
        RMapCache<String, ActiveUser> activeUsers = redissonClient.getMapCache("activeUsers");
        return new ArrayList<>(activeUsers.values());
    }

    @Override
    public Optional<ActiveUser> getActiveUser(String login) {
        RMapCache<String, ActiveUser> activeUsers = redissonClient.getMapCache("activeUsers");
        return Optional.ofNullable(activeUsers.get(login));
    }
}
