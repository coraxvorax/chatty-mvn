package ru.vorax.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.vorax.service.UnknownAccountException;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(UnknownAccountException.class)
    public ResponseEntity<String> handleException(UnknownAccountException ex) {
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
}
