package ru.vorax.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
@AllArgsConstructor
public class AccountsInfoResponse {

    private List<CommonInfo> commonInfos;

    public static AccountsInfoResponse of(List<CommonInfo> commonInfos) {
        return new AccountsInfoResponse(commonInfos);
    }

    public static AccountsInfoResponse of(CommonInfo commonInfo) {
        return new AccountsInfoResponse(Collections.singletonList(commonInfo));
    }
}
