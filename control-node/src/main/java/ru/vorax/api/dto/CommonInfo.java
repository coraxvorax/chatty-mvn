package ru.vorax.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class CommonInfo {
    private final String login;
    private final String username;
    private final String address;
    private final Long countOfMessages;

    public static CommonInfo of(String login, String username, String address, Long countOfMessages) {
        return new CommonInfo(login, username, address, countOfMessages);
    }
}
