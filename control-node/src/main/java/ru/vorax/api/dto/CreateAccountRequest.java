package ru.vorax.api.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class CreateAccountRequest {
    private String login;
    private String username;
    private String address;
}
