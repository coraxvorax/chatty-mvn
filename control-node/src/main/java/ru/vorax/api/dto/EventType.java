package ru.vorax.api.dto;

public enum EventType {
    SEND_MESSAGE, BROADCAST_MESSAGE
}