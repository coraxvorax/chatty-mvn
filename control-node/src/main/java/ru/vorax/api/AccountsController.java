package ru.vorax.api;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.vorax.api.dto.AccountsInfoResponse;
import ru.vorax.api.dto.CommonInfo;
import ru.vorax.api.dto.CreateAccountRequest;
import ru.vorax.service.AccountInfo;
import ru.vorax.service.AccountManager;
import ru.vorax.service.UnknownAccountException;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@RequestMapping("/api/v1/account")
@RequiredArgsConstructor
public class AccountsController {

    private final AccountManager accountManager;

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    @ApiOperation("Create account")
    @ResponseStatus(HttpStatus.CREATED)
    public void createAccount(@RequestBody CreateAccountRequest createAccountRequest) {
        accountManager.createAccount(createAccountRequest.getLogin(), createAccountRequest.getUsername(),
                createAccountRequest.getAddress());
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @ApiOperation("Get all accounts info")
    @ResponseStatus(HttpStatus.OK)
    public AccountsInfoResponse getAccounts() {
        return CommandsResponse.of(accountManager.getAccountsInfo());
    }

    @GetMapping(value = "/{login}", produces = APPLICATION_JSON_VALUE)
    @ApiOperation("Get account info")
    @ResponseStatus(HttpStatus.OK)
    public AccountsInfoResponse getAccount(@PathVariable("login") String login) throws UnknownAccountException {
        return CommandsResponse.of(accountManager.getAccountInfo(login));
    }

    @GetMapping(value = "/active", produces = APPLICATION_JSON_VALUE)
    @ApiOperation("Get active accounts info")
    @ResponseStatus(HttpStatus.OK)
    public AccountsInfoResponse getActiveAccounts() {
        return CommandsResponse.of(accountManager.getActiveAccountsInfo());
    }

    @GetMapping(value = "/active/logins", produces = APPLICATION_JSON_VALUE)
    @ApiOperation("Get active account logins")
    @ResponseStatus(HttpStatus.OK)
    public List<String> getActiveAccountLogins() {
        return accountManager.getActiveAccountLogins();
    }


    private static class CommandsResponse {

        private static AccountsInfoResponse of(List<AccountInfo> accountsInfo) {
            return AccountsInfoResponse.of(accountsInfo.stream()
                    .map(accountInfo -> CommonInfo.of(accountInfo.getLogin(), accountInfo.getUsername(),
                            accountInfo.getAddress(), accountInfo.getCountOfMessages()))
                    .collect(Collectors.toList()));
        }

        private static AccountsInfoResponse of(AccountInfo accountInfo) {
            return AccountsInfoResponse.of(CommonInfo.builder()
                    .login(accountInfo.getLogin())
                    .username(accountInfo.getUsername())
                    .address(accountInfo.getAddress())
                    .countOfMessages(accountInfo.getCountOfMessages())
                    .build());
        }
    }
}