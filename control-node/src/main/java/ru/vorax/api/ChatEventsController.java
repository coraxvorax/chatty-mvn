package ru.vorax.api;


import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.vorax.api.dto.EventType;
import ru.vorax.service.ChatEventManager;

@RestController
@RequestMapping("/api/v1/chat/events")
@RequiredArgsConstructor
public class ChatEventsController {

    private final ChatEventManager chatEventManager;

    @PostMapping
    @ApiOperation("Create account")
    @ResponseStatus(HttpStatus.CREATED)
    public void receiveChatEvent(@RequestParam("login") String login, @RequestParam("type") EventType type) {
        chatEventManager.receiveChatEvent(login, type.name());
    }
}
