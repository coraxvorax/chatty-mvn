package ru.vorax;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControlNodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ControlNodeApplication.class, args);
    }
}
