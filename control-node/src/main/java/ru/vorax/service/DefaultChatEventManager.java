package ru.vorax.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.vorax.storage.model.ChatEvent;
import ru.vorax.storage.model.ChatEventType;
import ru.vorax.storage.repository.AccountRepository;
import ru.vorax.storage.repository.ChatEventRepository;

import javax.transaction.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class DefaultChatEventManager implements ChatEventManager {

    private final ChatEventRepository chatEventRepository;
    private final AccountRepository accountRepository;

    @Override
    @Transactional
    public void receiveChatEvent(String login, String eventType) {
        accountRepository.findByLogin(login)
                .map(account -> chatEventRepository.save(ChatEvent.builder()
                        .account(account)
                        .accountLogin(login)
                        .chatEventType(ChatEventType.valueOf(eventType))
                        .build()))
                .orElseThrow(() -> new IllegalArgumentException(String.format("Unknown login %s", login)));
        LOG.info("Event successfully saved: eventType={}, account={}", eventType, login);
    }
}
