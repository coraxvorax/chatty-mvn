package ru.vorax.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.vorax.cache.ActiveUser;
import ru.vorax.cache.ActiveUsersCache;
import ru.vorax.storage.model.Account;
import ru.vorax.storage.repository.AccountRepository;
import ru.vorax.storage.repository.ChatEventRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
public class DefaultAccountManager implements AccountManager {

    private final AccountRepository accountRepository;
    private final ChatEventRepository chatEventRepository;
    private final ActiveUsersCache activeUsersCache;

    @Override
    @Transactional
    public void createAccount(String login, String username, String address) {
        accountRepository.save(Account.builder()
                .login(login)
                .username(username)
                .address(address)
                .build());
        LOG.info("Account successfully saved: login={}", login);
    }

    @Override
    public List<String> getActiveAccountLogins() {
        return activeUsersCache.getActiveUsers().stream()
                .map(ActiveUser::getLogin)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<AccountInfo> getActiveAccountsInfo() {
        List<String> activeUsers = activeUsersCache.getActiveUsers().stream()
                .map(ActiveUser::getLogin)
                .collect(Collectors.toList());
        return accountRepository.findByLogins(activeUsers).stream()
                .map(account -> AccountInfo.of(
                        account.getLogin(), account.getUsername(),
                        account.getAddress(), chatEventRepository.countByAccountLogin(account.getLogin())))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public AccountInfo getAccountInfo(String login) throws UnknownAccountException {
        Account account = accountRepository.findByLogin(login)
                .orElseThrow(() -> new UnknownAccountException(format("Account %s not found", login)));
        Long count = chatEventRepository.countByAccountLogin(login);
        return AccountInfo.builder()
                .login(login)
                .username(account.getUsername())
                .address(account.getAddress())
                .countOfMessages(count)
                .build();
    }

    @Override
    @Transactional
    public List<AccountInfo> getAccountsInfo() {
        return accountRepository.findAll().stream()
                .map(account -> AccountInfo.of(account.getLogin(),
                        account.getUsername(), account.getAddress(),
                        chatEventRepository.countByAccountLogin(account.getLogin())))
                .collect(Collectors.toList());
    }
}
