package ru.vorax.service;

import java.util.List;

public interface AccountManager {
    void createAccount(String login, String username, String address);

    List<String> getActiveAccountLogins();

    List<AccountInfo> getActiveAccountsInfo();

    AccountInfo getAccountInfo(String login) throws UnknownAccountException;

    List<AccountInfo> getAccountsInfo();
}
