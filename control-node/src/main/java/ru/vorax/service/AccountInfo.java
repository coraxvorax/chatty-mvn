package ru.vorax.service;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AccountInfo {
    private final String login;
    private final String username;
    private final String address;
    private final Long countOfMessages;

    public static AccountInfo of(String login, String username, String address, Long countOfMessages) {
        return new AccountInfo(login, username, address, countOfMessages);
    }
}
