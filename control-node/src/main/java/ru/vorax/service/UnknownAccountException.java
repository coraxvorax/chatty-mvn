package ru.vorax.service;

public class UnknownAccountException extends Exception {
    public UnknownAccountException(String message) {
        super(message);
    }

    public UnknownAccountException(String message, Throwable cause) {
        super(message, cause);
    }
}
