package ru.vorax.service;

public interface ChatEventManager {

    void receiveChatEvent(String login, String eventType);
}
