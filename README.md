# Chatty(distributed chat system) #

## Run chatty server

~~~
docker-compose -f docker-compose-dev.yml up
~~~

This runs two chat-nodes and one control-node accepting commands.
It also runs redis, which acts as a cache of active users and as a topic

## Run chatty client

Connecting to the first node:

~~~
java -jar chat-client-1.0-SNAPSHOT.jar --chat-node-port=8666
~~~

Connecting to the second node:

~~~
java -jar chat-client-1.0-SNAPSHOT.jar --chat-node-port=8667
~~~

## Scaling
### Description
In this configuration, 2 nodes are started (the number of nodes can be arbitrary),
on which the user connections are registered. Each node has a list of user connections.
Simple scaling can be done through a proxy server, such as nginx, and scale,
e.g. by the number of connections on each node.

### Message sending/receiving mechanism

The mechanism of sending/receiving messages is implemented using topics. In this case, definition
of a node's topic is done by determining on which node there is a user connection.
Each node listens to topics such as "pushMessages.shard1" and "broadcastMessages".

## Scenarios

### Registering a user and adding to the chat room

When you start the chat-client, a message will be displayed:

~~~
Enter login:
~~~

After entering the login, it will check if the user exists in the database. If it doesn't, then control-node will be
control-node to create the user and then a request will be sent to the chat-node to add
user to the chat. After the connection, you will be able to enter messages.

### Sending a broadcast message

After registering and being added to the chat room, you can send a broadcast message:

~~~
Hell world!
~~~

### Sending a message to a specific user

After registering and adding to the chat room, you can send a message to a specific user:

~~~
@user1 Hello!
~~~

The recipient of the message will only be user user1

### Sending a command

After registering and adding to the chat room, you can send a command:

~~~
/active-users
~~~

This will display only the logins of active users.
You can also send a command:

~~~
/active-users-detailed
~~~

This will display detailed information on each active user

## Active user cache

After a user sends a message, it is added to the cache of active users. The cache entry is stored for 10 minutes.
If within 10 minutes the user has not sent messages, the entry in the cache is deleted and it becomes inactive

## Sending chat events

After the user sends a message, an asynchronous request is made to the control-node to add a message sending event.
This information is used to calculate the number of messages sent by the user


## Swagger on control-node

It is possible to use a swagger on a control-node:

~~~
http://localhost:8668/swagger-ui.html
~~~